package com.groundgurus.day2.exercise;

public class SupermarketMain {
	public static void main(String[] args) {
		Supermarket hypermart = new Supermarket("Hypermart");
		
		// load the items in the supermarket
		hypermart.loadItems();
		
		Customer johnDoe = new Customer("John Doe", 500.00);
		
		Item[] itemsBought = new Item[14];
		
		// get 12 eggs
		for (int i = 0; i < 12; i++) {
			itemsBought[i] = hypermart.getItem("Egg");
		}
		
		itemsBought[12] = hypermart.getItem("Butter");
		itemsBought[13] = hypermart.getItem("Pack of Bread");
		
		double change = johnDoe.checkout(itemsBought);
		
		System.out.println("Change: " + change);
	}
}
