package com.groundgurus.day2.exercise;

public class Item {
	String name;
	double prize;
	Size size;
	
	public Item(String name, double prize, Size size) {
		this.name = name;
		this.prize = prize;
		this.size = size;
	}
}
