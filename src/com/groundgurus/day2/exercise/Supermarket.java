package com.groundgurus.day2.exercise;

public class Supermarket {
	String name;
	Item[] items = new Item[1_000];
	int count;
	
	public Supermarket(String name) {
		this.name = name;
	}

	public void loadItems() {
		// 12 eggs
		for (int i = 1; i <= 12; i++) {
			addItem(new Item("Egg", 7.50, Size.M));
		}
		
		// butter
		addItem(new Item("Butter", 20.00, Size.S));
		
		// bread
		addItem(new Item("Pack of Bread", 40.00, Size.L));
	}
	
	public Item getItem(String itemName) {
		for (int i = 0; i < count; i++) {
			Item temp = items[i];
			if (temp != null && itemName.equals(temp.name)) {
				items[i] = null;
				return temp;
			}
		}
		
		return null;
	}
	
	public void addItem(Item item) {
		items[count++] = item;
	}
}
