package com.groundgurus.day2.exercise;

public class Customer {
	String name;
	double money;
	
	public Customer(String name, double money) {
		this.name = name;
		this.money = money;
	}
	
	public double checkout(Item[] itemsBought) {
		double change = 0.0;
		
		// get the total price
		double totalPrice = 0.0;
		for (int i = 0; i < itemsBought.length; i++) {
			totalPrice = totalPrice + itemsBought[i].prize;
		}

		change = money - totalPrice;
		money = change;
		
		return change;
	}
}
