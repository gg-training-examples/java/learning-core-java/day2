package com.groundgurus.day2;

public class ShopMain {
	public static void main(String[] args) {
		// create a shop object with initial values
		Shop cakeShop = new Shop("Red Ribbon", 
				new String[] {"Triple Chocolate", "Vanilla"});
		
		Shop flowerShop = new Shop("Rose Flower Shop",
				new String[] {"Rose", "Daisy"});
		flowerShop.name = "Rose Flower Shop"; // set
		
		cakeShop.printDetails();
		
		System.out.println();
		
		flowerShop.printDetails();
	}
}
