package com.groundgurus.day2.videogames;

public class VideoGameCharacter {
	public String name;
	public Alignment alignment;
	
	public void printDetails() {
		System.out.println("Name: " + name);
		System.out.println("Alignment: " + alignment);
	}
}
