package com.groundgurus.day2.videogames;

public class VideoGameMain {
	public static void main(String[] args) {
		DragonBallGame dragonBallGame = new DragonBallGame();
		
		VideoGameCharacter sonGoku = new VideoGameCharacter();
		sonGoku.name = "Son Goku";
		sonGoku.alignment = Alignment.GOOD;
		dragonBallGame.addCharacter(sonGoku);
		
		VideoGameCharacter vegeta = new VideoGameCharacter();
		vegeta.name = "Vegeta";
		vegeta.alignment = Alignment.NEUTRAL;
		dragonBallGame.addCharacter(vegeta);
		
		dragonBallGame.viewCharacterDetails();
	}
}
