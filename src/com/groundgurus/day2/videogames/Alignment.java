package com.groundgurus.day2.videogames;

public enum Alignment {
	GOOD, NEUTRAL, EVIL
}
